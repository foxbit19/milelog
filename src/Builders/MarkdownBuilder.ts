namespace Builders {
    class MarkdownBuilder implements Builder {
        getTitle(title: string): string {
            return '# ' + title;
        }

        getSection(sectionName: string): string {
            return '##' + sectionName;
        }

        getSubSection(subSectionName: string): string {
            return '###' + subSectionName;
        }

        getListElement(elementDescription: string): string {
            return ' - ' + elementDescription
        }
    }
}