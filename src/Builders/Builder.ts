namespace Builders {
    export interface Builder {
        getTitle(title: string): string;
        getSection(sectionName: string): string;
        getSubSection(subSectionName: string): string;
        getListElement(elementDescription: string): string;
    }
}