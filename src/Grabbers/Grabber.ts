namespace Grabbers {
    export interface Grabber{
        getMilestones(): Array<Milestone>
        getMilestoneTasks(milestone: Milestone): Array<Task>
    }
}